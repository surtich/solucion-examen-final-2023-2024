# Examen Final Evaluación Diseño Web y Desarrollo Cliente

# Día 13/03/2024 Tiempo: 5 horas

- Nota: Cada pregunta se valorará como bien o como mal (valoraciones intermedias serán excepcionales).
- Nota2: Cada pregunta computará únicamente en el examen al que pertenezca (diseño o desarrollo). Para resolver los apartados del examen de diseño no se debe usar JavaScript.
- Nota3: Para aprobar cada examen hay que obtener una puntuación mínima de 5 puntos en ese examen.
- Nota4: Organice su tiempo. Si no consigue resolver un apartado pase al siguiente. El examen consta de apartados de diseño y de desarrollo que se pueden resolver por separado. Si un apartado depende de otro que no sabe resolver, siempre puede dar una solución que aunque no sea correcta, le permita seguir avanzando.
- Nota5: Para que una solución sea correcta, no sólo hay que conseguir que haga lo que se pide, sino que además **todo lo que funcionaba lo tiene que seguir haciendo**. La solución debe estar implementada según las prácticas de código limpio explicadas en clase. Esto incluye JavaScript, CSS y HTML.
- Nota6: Lea completamente el examen antes de empezar y comience por lo que le parezca más fácil.
- Nota7: No se permite utilizar ninguna librería que no esté ya incluida en el fichero "package.json".

Pasos previos antes de empezar

- Clone el repositorio del enunciado

```bash
git clone https://gitlab.com/surtich/enunciado-examen-final-2023-2024
```

- Vaya al directorio del proyecto

```bash
cd enunciado-examen-final-2023-2024
```

- Si no lo hecho anteriormente, configure su usuario de git:

```bash
git config user.name "Sustituya por su nombre y apellidos"
git config user.email "Sustituya por su correo electrónico"
```

- Cree un _branch_ con su nombre y apellidos separados con guiones (no incluya mayúsculas, acentos o caracteres no alfabéticos, excepción hecha de los guiones). Ejemplo:

```bash
    git checkout -b fulanito-perez-gomez
```

- Compruebe que está en la rama correcta:

```bash
    git status
```

- Suba la rama al repositorio remoto:

```bash
    git push origin <nombre-de-la-rama-dado-anteriormente>
```

- Instale las dependencias y arranque el cliente:

```bash
    npm install
    npm run dev -- --open
```

- En otro terminal arranque el servidor:

```bash
    npm install
    npm run serve-json
```

Navegue a [http://localhost:5173/](http://localhost:5173/)

Para ver los `endpoints` navegue a [http://localhost:4000/](http://localhost:4000/)

- Dígale al profesor que ya ha terminado para que compruebe que todo es correcto y desconecte la red.

# Introducción

Los exámenes constan de un único ejercicio dividido en apartados. Se pretende que se puedan puntuar los vídeos entre 1 y 5. Para ello se ha creado un componente \<Star\> en "lib/Star.svelte". El componente ya se ha añadido a \<Embed\> para poder cambiar la puntuación de un vídeo y a \<VideoList\> para mostrar la puntuación.

Este es el resultado final que se espera:

![Image 0](./img/0.gif)

# Examen de diseño

1.-(1 punto) Posicione adecuadamente el componente \<Start\> en \<Embed\>.

![Image 1](./img/1.png)

2.-(1 punto) Posicione adecuadamente el componente \<Start\> en \<VideoList\>.

![Image 2](./img/2.png)

3.- (1 punto) Las estrellas tendrán ojos.

Nota: Puede hacer esto usando bordes y colores de fondo.

![Image 3](./img/3.png)

4.- (1 punto) Los ojos de la quinta estrella serán diferentes.

![Image 4](./img/4.png)

5.- (3 puntos) Cada estrella tendrá un boca.

Nota: Excepto la segunda estrella, que no tiene boca.

![Image 5](./img/5.png)

6.- Al recargar la página, las estrellas cuatro y cinco se animarán.

6.1.- (1 punto) Se muestra ralentizada la animación de la estrella 4.

![Image 6.1](./img/6.1.gif)

6.2.- (2 puntos) Se muestra ralentizada la animación de la estrella 5.

![Image 6.2](./img/6.2.gif)

# Examen de desarrollo

7.-(1 punto) Las estrellas se crearán con un bucle y tendrán color negro. Al pulsar sobre una estrella cambiará su color a amarillo y el resto serán negras.

![Image 7](./img/7.gif)

8.-(1 punto) La estrella seleccionada, tendrá los ojos y la boca que le corresponda.

Nota: Si no ha hecho el examen de diseño, cada estrella tendrá un color que será seleccionado según la posición que ocupe en el siguiente array.

const colors = ["red", "orange", "yellow", "greenyellow", "green"];

Nota: Para puntuar en este apartado, es necesario que se hayan creado las estrellas con un array (apartado 7).

9.- (2 puntos, difícil) Al seleccionar una estrella, se recorrerán todas las estrellas que haya entre la anterior selección y la actual selección.

Nota: La imagen muestra el resultado ralentizado.

![Image 9](./img/9.gif)

10.- (1 punto) Al seleccionar una estrella se almacenará la puntuación en el servidor.

Nota: Observe que en la imagen los dos primeros vídeos tienen puntuación y los dos siguientes no la tienen.

![Image 10](./img/10.png)

11.- (2 puntos) Al cargar los vídeos en \<VideoList \> se mostrará el componente \<Star\> con la puntuación que tenga el vídeo en aquellos vídeos que tengan puntuación. Si un vídeo no tiene puntuación, no se mostrará el componente \<Star\>.

Nota: Observe que se muestra una única estrella en los dos primeros vídeos por tener puntuación puntuación y ninguna en los vídeos siguientes ya que no tienen puntuación.
Nota: En \<VideoList \> no se pueden puntuar vídeos, únicamente se muestran las puntuaciones.

![Image 11](./img/11.png)

12.- (1 punto) Al seleccionar un vídeo, se mostrará la puntuación que tenga o ninguna si no se ha valorado.

Nota: Los vídeos se cargan del servidor en el "layout" y no es correcto volver a leerlos del servidor incluso si alguno ha cambiado de puntuación.

![Image 12](./img/12.gif)

13.- (1 punto) Al cambiar la puntuación de un vídeo, se actualizará el componente \<VideoList\> del panel lateral.

![Image 13](./img/13.gif)

14.- (1 punto, difícil) Si un vídeo tiene puntuación y se pulsa sobre esa misma puntuación, el vídeo deja de estar puntuado. Se debe eliminar la selección de la estrella en \<Embed\> y eliminar la estrella en \<VideoList\>. También se debe eliminar la puntuación en el servidor.

Nota: Para hacer esto, no se deben volver a leer los datos del vídeo del servidor.
Nota: Puede que aquí resulten útiles los eventos personalizados.

![Image 14](./img/14.gif)

# Para entregar

- Ejecute el siguiente comando para comprobar que está en la rama correcta y ver los ficheros que ha cambiado:

```bash
    git status
```

- Prepare los cambios para que se añadan al repositorio local:

```bash
    git add .
    git commit -m "completed exam"
```

- Compruebe que no tiene más cambios que incluir:

```bash
    git status
```

- Dígale al profesor que va a entregar el examen.

- Conecte la red y ejecute el siguiente comando:

```bash
    git push origin <nombre-de-la-rama>
```

- Abandone el aula en silencio.
