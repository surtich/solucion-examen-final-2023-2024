export const load = async ({ fetch }) => {
	const response = await fetch('http://localhost:4000/videos');
	const videos = await response.json();
	const categories = Array.from(new Set(videos.map((video) => video.category))).sort(
		(a, b) => b - a
	);
	return {
		videos,
		categories
	};
};
