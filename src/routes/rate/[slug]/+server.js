import { json } from '@sveltejs/kit';

export async function POST({ params, request }) {
	const { slug } = params;
	const { rating } = await request.json();
	await fetch(`http://localhost:4000/videos/${slug}`, {
		method: 'PATCH',
		body: JSON.stringify({ rating })
	});
	return json({
		message: 'Successfully rated video!'
	});
}
