import { json } from '@sveltejs/kit';
import { promiseTimeout } from '../../../lib/utils.js';

export async function GET({ params }) {
	const { slug } = params;
	const response = await fetch(`http://localhost:4000/descriptions/${slug}`);
	const { description } = await response.json();
	return promiseTimeout(json({ [slug]: description }));
}
