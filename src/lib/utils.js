export function formatDate(date) {
	return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date
		.getDate()
		.toString()
		.padStart(2, '0')}`;
}

export function promiseTimeout(value = true, time = 1000) {
	return new Promise(function (resolve) {
		setTimeout(function () {
			resolve(value);
		}, time);
	});
}
