import { writable } from 'svelte/store';

const cacheStore = writable({
	descriptions: {}
});

export default cacheStore;
